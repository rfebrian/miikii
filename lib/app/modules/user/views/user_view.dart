import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:miikii/app/controllers/auth_controller.dart';

import '../controllers/user_controller.dart';

class UserView extends GetView<UserController> {
  final authC = Get.find<AuthController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('UserView'),
        centerTitle: true,
        actions: [
          IconButton(onPressed: () => authC.logout(), icon: Icon(Icons.logout))
        ],
      ),
      body: Center(
        child: Text(
          'UserView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
