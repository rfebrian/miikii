import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddTransactionController extends GetxController {
  late TextEditingController nameC;
  late TextEditingController typeC;
  late TextEditingController priceC;

  FirebaseFirestore firestore = FirebaseFirestore.instance;

  void addTransaction(
    String name,
    String type,
    String price,
  ) async {
    CollectionReference transactions = firestore.collection("transactions");
    try {
      await transactions.add({
        "name": name,
        "type": type,
        "price": price,
      });
      Get.defaultDialog(
          title: "Berhasil",
          middleText: "Transaksi sudah berhasil",
          onConfirm: () {
            nameC.clear();
            typeC.clear();
            priceC.clear();
            Get.back();
            Get.back();
          },
          textConfirm: "Ok");
    } catch (e) {
      Get.defaultDialog(
          title: "Terjadi Kesalahan",
          middleText: "Transaksi tidak dapat dilakukan saat ini",
          onConfirm: () {
            Get.back();
            Get.back();
          },
          textConfirm: "Ok");
    }
  }

  @override
  void onInit() {
    nameC = TextEditingController();
    typeC = TextEditingController();
    priceC = TextEditingController();
  }

  @override
  void onClose() {
    nameC.dispose();
    typeC.dispose();
    priceC.dispose();
    super.onClose();
  }

  Stream<QuerySnapshot<Object?>> streamDataTransaction() {
    CollectionReference transactions = firestore.collection("transactions");
    return transactions.snapshots();
  }

  void deleteTransaction(String docID) {
    DocumentReference docRef = firestore.collection("transactions").doc(docID);
    try {
      Get.defaultDialog(
          title: "Hapus Data",
          middleText: "Apakah data akan dihapus ?",
          onConfirm: () async {
            await docRef.delete();
            Get.back();
          },
          textConfirm: "Yes",
          textCancel: "No");
    } catch (e) {
      print(e);
      Get.defaultDialog(
        title: "Terjadi Kesalahan",
        middleText: "Tidak dapat menghapus data ini",
      );
    }
  }
}
