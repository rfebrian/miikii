import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:miikii/app/controllers/auth_controller.dart';
import 'package:miikii/app/routes/app_pages.dart';

import '../controllers/add_transaction_controller.dart';

class AddTransactionView extends GetView<AddTransactionController> {
  final authC = Get.find<AuthController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            height: MediaQuery.of(context).size.height / 1,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: IconButton(
                          alignment: Alignment.center,
                          onPressed: () => Get.back(),
                          icon: Icon(Icons.arrow_back)),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height / 8,
                      width: MediaQuery.of(context).size.width / 3,
                      decoration: BoxDecoration(
                          color: Color(0xff65B73B),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(30),
                              bottomRight: Radius.circular(30))),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 20),
                      child: IconButton(
                          alignment: Alignment.center,
                          onPressed: () => authC.logout(),
                          icon: Icon(Icons.logout)),
                    )
                  ],
                ),
                _TransactionForm(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _TransactionForm() {
    return Container(
      height: 285,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            TextField(
              controller: controller.nameC,
              autocorrect: false,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(labelText: "Nama Tabungan"),
            ),
            TextField(
              controller: controller.typeC,
              autocorrect: false,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(labelText: "Type Tabungan"),
            ),
            TextField(
              controller: controller.priceC,
              autocorrect: false,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(labelText: "Jumlah Transaksi"),
            ),
            SizedBox(
              height: 15,
            ),
            ElevatedButton(
                onPressed: () => controller.addTransaction(
                    controller.nameC.text,
                    controller.typeC.text,
                    controller.priceC.text),
                child: Text("Tambah")),
            SizedBox(
              height: 10,
            )
          ],
        ),
      ),
    );
  }
}
