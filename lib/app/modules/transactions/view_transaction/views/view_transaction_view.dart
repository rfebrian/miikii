import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:miikii/app/controllers/auth_controller.dart';
import 'package:miikii/app/modules/transactions/view_transaction/controllers/view_transaction_controller.dart';
import 'package:miikii/app/routes/app_pages.dart';

class ViewTransactionView extends GetView<ViewTransactionController> {
  final authC = Get.find<AuthController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height / 1,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: IconButton(
                        alignment: Alignment.center,
                        onPressed: () => Get.toNamed(Routes.ADD_TRANSACTION),
                        icon: Icon(Icons.add)),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height / 8,
                    width: MediaQuery.of(context).size.width / 3,
                    decoration: BoxDecoration(
                        color: Color(0xff65B73B),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(30),
                            bottomRight: Radius.circular(30))),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: IconButton(
                        alignment: Alignment.center,
                        onPressed: () => authC.logout(),
                        icon: Icon(Icons.logout)),
                  )
                ],
              ),
              SizedBox(
                height: 30,
              ),
              _TransactionData()
            ],
          ),
        ),
      ),
    );
  }

  _TransactionData() {
    var streamBuilder = StreamBuilder<QuerySnapshot<Object?>>(
        stream: controller.streamDataTransaction(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.active) {
            var listAllDocs = snapshot.data!.docs;
            return ListView.builder(
                itemCount: listAllDocs.length,
                itemBuilder: (context, index) => ListTile(
                      onTap: () => Get.toNamed(Routes.EDIT_TRANSACTION,
                          arguments: listAllDocs[index].id),
                      title: Text(
                        "${(listAllDocs[index].data() as Map<String, dynamic>)["name"]}",
                      ),
                      subtitle: Text(
                        "${(listAllDocs[index].data() as Map<String, dynamic>)["type"]}",
                      ),
                      trailing: IconButton(
                          onPressed: () => controller
                              .deleteTransaction(listAllDocs[index].id),
                          icon: Icon(Icons.delete)),
                    ));
          }
          return Center(child: CircularProgressIndicator());
        });
    return Container(
      height: 520,
      child: streamBuilder,
    );
  }
}
