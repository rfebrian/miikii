import 'package:get/get.dart';
import 'package:miikii/app/modules/transactions/edit_transaction/controllers/edit_transaction_controller.dart';
import 'package:miikii/app/modules/transactions/view_transaction/controllers/view_transaction_controller.dart';

class ViewTransactionBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ViewTransactionController>(
      () => ViewTransactionController(),
    );
  }
}
