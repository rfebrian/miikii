import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ViewTransactionController extends GetxController {
  late TextEditingController nameC;
  late TextEditingController typeC;
  late TextEditingController priceC;

  FirebaseFirestore firestore = FirebaseFirestore.instance;

  Future<DocumentSnapshot<Object?>> getData(String docID) async {
    DocumentReference docRef = firestore.collection("transactions").doc(docID);
    return docRef.get();
  }

  void editTransaction(
      String name, String type, String price, String docID) async {
    DocumentReference docData = firestore.collection("transactions").doc(docID);
    try {
      await docData.update({
        "name": name,
        "type": type,
        "price": int.parse(price),
      });
      Get.defaultDialog(
          title: "Berhasil",
          middleText: "Ubah transaksi sudah berhasil",
          onConfirm: () {
            nameC.clear();
            typeC.clear();
            priceC.clear();
            Get.back();
            Get.back();
          },
          textConfirm: "Ok");
    } catch (e) {
      print(e);
      Get.defaultDialog(
          title: "Terjadi Kesalahan",
          middleText: "Ubah transaksi tidak dapat dilakukan saat ini",
          onConfirm: () {
            Get.back();
            Get.back();
          },
          textConfirm: "Ok");
    }
  }

  @override
  void onInit() {
    nameC = TextEditingController();
    typeC = TextEditingController();
    priceC = TextEditingController();
  }

  @override
  void onClose() {
    nameC.dispose();
    typeC.dispose();
    priceC.dispose();
    super.onClose();
  }

  Stream<QuerySnapshot<Object?>> streamDataTransaction() {
    CollectionReference transactions = firestore.collection("transactions");
    return transactions.snapshots();
  }

  void deleteTransaction(String docID) {
    DocumentReference docRef = firestore.collection("transactions").doc(docID);
    try {
      Get.defaultDialog(
          title: "Hapus Data",
          middleText: "Apakah data akan dihapus ?",
          onConfirm: () async {
            await docRef.delete();
            Get.back();
          },
          textConfirm: "Yes",
          textCancel: "No");
    } catch (e) {
      print(e);
      Get.defaultDialog(
        title: "Terjadi Kesalahan",
        middleText: "Tidak dapat menghapus data ini",
      );
    }
  }
}
