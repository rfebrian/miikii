import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:miikii/app/controllers/auth_controller.dart';

import '../controllers/signup_controller.dart';

class SignupView extends GetView<SignupController> {
  final authC = Get.find<AuthController>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: true,
      top: true,
      child: Scaffold(
          body: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height / 8 ,
                  width: MediaQuery.of(context).size.width / 3 ,
                  decoration: BoxDecoration(
                      color: Color(0xff65B73B),
                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30))
                  ),
                ),
                SizedBox(height: 25),
                Container(
                  height: MediaQuery.of(context).size.height / 8 ,
                  width: MediaQuery.of(context).size.width / 1.2 ,
                  decoration: BoxDecoration(
                      color: Color(0xffFD7439),
                      borderRadius: BorderRadius.all(Radius.circular(30))
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20, top: 40, right: 70, left: 70),
                  child: TextField(
                    controller: controller.emailC,
                    decoration: InputDecoration(labelText: "Email"),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 70, left: 70),
                  child: TextFormField(

                    obscureText: true,
                    controller: controller.passC,
                    decoration: InputDecoration(labelText: "Password"),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  height: MediaQuery.of(context).size.height / 14 ,
                  width: MediaQuery.of(context).size.height / 2.5 ,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(),
                      onPressed: () =>
                          authC.signup(controller.emailC.text, controller.passC.text),
                      child: Text("Register")),
                ),
                SizedBox(height: 140,),
                Container(
                  alignment: Alignment.center,
                  height: MediaQuery.of(context).size.height / 13 ,
                  width: MediaQuery.of(context).size.width / 1.1 ,
                  decoration: BoxDecoration(
                      color: Color(0xff65B73B),
                      borderRadius: BorderRadius.all(Radius.circular(30))
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Sudah punya akun ?", style: TextStyle(color: Colors.white),),
                      TextButton(onPressed: () => Get.back(), child: Text("Login"))
                    ],
                  ),
                )
              ],
            ),
          )),
    );
  }
}


