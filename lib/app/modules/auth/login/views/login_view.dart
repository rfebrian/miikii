import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:miikii/app/controllers/auth_controller.dart';
import 'package:miikii/app/routes/app_pages.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  final authC = AuthController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: true,
      top: true,
      child: Scaffold(
          body: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height / 8 ,
                  width: MediaQuery.of(context).size.width / 3 ,
                  decoration: BoxDecoration(
                    color: Color(0xff65B73B),
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30))
                  ),
                ),
                SizedBox(height: 25),
                Container(
                  height: MediaQuery.of(context).size.height / 8 ,
                  width: MediaQuery.of(context).size.width / 1.2 ,
                  decoration: BoxDecoration(
                      color: Color(0xffFD7439),
                      borderRadius: BorderRadius.all(Radius.circular(30))
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20, top: 40, right: 70, left: 70),
                  child: TextField(
                    controller: controller.emailC,
                    decoration: InputDecoration(labelText: "Email"),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 70, left: 70),
                  child: TextField(
                    obscureText: true,
                    controller: controller.passC,
                    decoration: InputDecoration(labelText: "Password"),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: const EdgeInsets.only(left: 62),
                  alignment: Alignment.centerLeft,
                  child: TextButton(
                      onPressed: () => Get.toNamed(Routes.RESET_PASSWORD),
                      child: Text("Lupa Password ?")),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: MediaQuery.of(context).size.height / 14 ,
                  width: MediaQuery.of(context).size.height / 2.5 ,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(),
                      onPressed: () =>
                          authC.login(controller.emailC.text, controller.passC.text),
                      child: Text("Login")),
                ),
                SizedBox(height: 100,),
                Container(
                  alignment: Alignment.center,
                  height: MediaQuery.of(context).size.height / 13 ,
                  width: MediaQuery.of(context).size.width / 1.1 ,
                  decoration: BoxDecoration(
                      color: Color(0xff65B73B),
                      borderRadius: BorderRadius.all(Radius.circular(30))
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Belum punya akun ?", style: TextStyle(color: Colors.white),),
                      TextButton(
                          onPressed: () => Get.toNamed(Routes.SIGNUP),
                          child: Text("Register"))
                    ],
                  ),
                )
              ],
            ),
          )),
    );
  }
}
