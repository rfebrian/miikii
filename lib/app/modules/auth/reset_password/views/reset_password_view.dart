import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:miikii/app/controllers/auth_controller.dart';

import '../controllers/reset_password_controller.dart';

class ResetPasswordView extends GetView<ResetPasswordController> {
  @override
  final authC = Get.find<AuthController>();
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Reset Password'),
          centerTitle: true,
        ),
        body: Column(
          children: [
            TextField(
              controller: controller.emailC,
              decoration: InputDecoration(labelText: "Email"),
            ),
            SizedBox(
              height: 30,
            ),
            ElevatedButton(
                onPressed: () => authC.resetPassword(
                      controller.emailC.text,
                    ),
                child: Text("Reset")),
            Row(
              children: [
                Text("Sudah punya akun"),
                TextButton(onPressed: () => Get.back(), child: Text("Login"))
              ],
            )
          ],
        ));
  }
}
