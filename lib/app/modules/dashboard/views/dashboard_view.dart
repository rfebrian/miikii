import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:miikii/app/modules/escrows/add_escrow/views/add_escrow_view.dart';
import 'package:miikii/app/modules/escrows/view_escrow/views/view_escrow_view.dart';
import 'package:miikii/app/modules/home/views/home_view.dart';
import 'package:miikii/app/modules/transactions/add_transaction/views/add_transaction_view.dart';
import 'package:miikii/app/modules/transactions/view_transaction/views/view_transaction_view.dart';
import 'package:miikii/app/modules/user/views/user_view.dart';

import '../controllers/dashboard_controller.dart';

class DashboardView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DashboardController>(
      builder: (controller) {
        return Scaffold(
          body: SafeArea(
              child: IndexedStack(
            index: controller.tabIndex,
            children: [
              HomeView(),
              ViewTransactionView(),
              ViewEscrowView(),
            ],
          )),
          bottomNavigationBar: BottomNavigationBar(
            unselectedItemColor: Colors.black,
            selectedItemColor: Colors.redAccent,
            onTap: controller.changeTabIndex,
            currentIndex: controller.tabIndex,
            showUnselectedLabels: false,
            showSelectedLabels: false,
            items: [
              _bottomNavigationBarItem(icon: Icons.home, label: 'Dashboard'),
              _bottomNavigationBarItem(
                  icon: Icons.attach_money_rounded, label: 'Transaksi'),
              _bottomNavigationBarItem(
                  icon: Icons.credit_card_rounded, label: 'Rekening'),
            ],
          ),
        );
      },
    );
  }

  _bottomNavigationBarItem({IconData? icon, String? label}) {
    return BottomNavigationBarItem(icon: Icon(icon), label: label);
  }
}
