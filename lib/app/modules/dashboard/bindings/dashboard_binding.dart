import 'package:get/get.dart';
import 'package:miikii/app/modules/escrows/add_escrow/controllers/add_escrow_controller.dart';
import 'package:miikii/app/modules/home/controllers/home_controller.dart';
import 'package:miikii/app/modules/transactions/add_transaction/controllers/add_transaction_controller.dart';
import 'package:miikii/app/modules/transactions/view_transaction/controllers/view_transaction_controller.dart';
import 'package:miikii/app/modules/user/controllers/user_controller.dart';

import '../controllers/dashboard_controller.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DashboardController>(
      () => DashboardController(),
    );
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
    Get.lazyPut<ViewTransactionController>(
      () => ViewTransactionController(),
    );
    Get.lazyPut<AddTransactionController>(
      () => AddTransactionController(),
    );
    Get.lazyPut<UserController>(
      () => UserController(),
    );
    Get.lazyPut<AddEscrowController>(
      () => AddEscrowController(),
    );
  }
}
