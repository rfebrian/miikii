import 'package:get/get.dart';
import 'package:miikii/app/modules/escrows/add_escrow/controllers/add_escrow_controller.dart';

class AddEscrowBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AddEscrowController>(
      () => AddEscrowController(),
    );
  }
}
