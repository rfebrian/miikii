import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ViewEscrowController extends GetxController {
  late TextEditingController nameC;
  late TextEditingController typeC;
  late TextEditingController balanceC;

  FirebaseFirestore firestore = FirebaseFirestore.instance;

  void addEscrow(
    String name,
    String type,
    String balance,
  ) async {
    CollectionReference escrows = firestore.collection("escrows");
    try {
      await escrows.add({
        "name": name,
        "type": type,
        "balance": balance,
      });
      Get.defaultDialog(
          title: "Berhasil",
          middleText: "Rekening berhasil di tambahkan",
          onConfirm: () {
            nameC.clear();
            typeC.clear();
            balanceC.clear();
            Get.back();
            Get.back();
          },
          textConfirm: "Ok");
    } catch (e) {
      Get.defaultDialog(
          title: "Terjadi Kesalahan",
          middleText: "Tidak dapat menambahkan rekening",
          onConfirm: () {
            Get.back();
            Get.back();
          },
          textConfirm: "Ok");
    }
  }

  @override
  void onInit() {
    nameC = TextEditingController();
    typeC = TextEditingController();
    balanceC = TextEditingController();
  }

  @override
  void onClose() {
    nameC.dispose();
    typeC.dispose();
    balanceC.dispose();
    super.onClose();
  }

  Stream<QuerySnapshot<Object?>> streamDataTransaction() {
    CollectionReference escrows = firestore.collection("escrows");
    return escrows.snapshots();
  }

  void deleteTransaction(String docID) {
    DocumentReference docRef = firestore.collection("escrows").doc(docID);
    try {
      Get.defaultDialog(
          title: "Hapus Data",
          middleText: "Apakah data akan dihapus ?",
          onConfirm: () async {
            await docRef.delete();
            Get.back();
          },
          textConfirm: "Yes",
          textCancel: "No");
    } catch (e) {
      print(e);
      Get.defaultDialog(
        title: "Terjadi Kesalahan",
        middleText: "Tidak dapat menghapus data ini",
      );
    }
  }
}
