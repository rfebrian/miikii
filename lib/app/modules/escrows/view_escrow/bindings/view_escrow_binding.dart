import 'package:get/get.dart';
import 'package:miikii/app/modules/escrows/view_escrow/controllers/view_escrow_controller.dart';

class ViewEscrowBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ViewEscrowController>(
      () => ViewEscrowController(),
    );
  }
}
