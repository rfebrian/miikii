import 'package:get/get.dart';

import '../controllers/edit_escrow_controller.dart';

class EditEscrowBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EditEscrowController>(
      () => EditEscrowController(),
    );
  }
}
