import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class EditEscrowController extends GetxController {
  late TextEditingController nameC;
  late TextEditingController typeC;
  late TextEditingController balanceC;

  FirebaseFirestore firestore = FirebaseFirestore.instance;

  Future<DocumentSnapshot<Object?>> getData(String docID) async {
    DocumentReference docRef = firestore.collection("escrows").doc(docID);
    return docRef.get();
  }

  void editEscrow(
      String name, String type, String balance, String docID) async {
    DocumentReference docData = firestore.collection("escrows").doc(docID);
    try {
      await docData.update({
        "name": name,
        "type": type,
        "price": int.parse(balance),
      });
      Get.defaultDialog(
          title: "Berhasil",
          middleText: "Ubah transaksi sudah berhasil",
          onConfirm: () {
            nameC.clear();
            typeC.clear();
            balanceC.clear();
            Get.back();
            Get.back();
          },
          textConfirm: "Ok");
    } catch (e) {
      print(e);
      Get.defaultDialog(
          title: "Terjadi Kesalahan",
          middleText: "Ubah transaksi tidak dapat dilakukan saat ini",
          onConfirm: () {
            Get.back();
            Get.back();
          },
          textConfirm: "Ok");
    }
  }

  @override
  void onInit() {
    nameC = TextEditingController();
    typeC = TextEditingController();
    balanceC = TextEditingController();
  }

  @override
  void onClose() {
    nameC.dispose();
    typeC.dispose();
    balanceC.dispose();
    super.onClose();
  }
}
