import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:miikii/app/controllers/auth_controller.dart';
import 'package:miikii/app/routes/app_pages.dart';

import '../controllers/edit_escrow_controller.dart';

class EditEscrowView extends GetView<EditEscrowController> {
  final authC = Get.find<AuthController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            height: MediaQuery.of(context).size.height / 1,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: IconButton(
                          alignment: Alignment.center,
                          onPressed: () => Get.back(),
                          icon: Icon(Icons.arrow_back)),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height / 8,
                      width: MediaQuery.of(context).size.width / 3,
                      decoration: BoxDecoration(
                          color: Color(0xff65B73B),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(30),
                              bottomRight: Radius.circular(30))),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 20),
                      child: IconButton(
                          alignment: Alignment.center,
                          onPressed: () => authC.logout(),
                          icon: Icon(Icons.logout)),
                    )
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                FutureBuilder<DocumentSnapshot<Object?>>(
                  future: controller.getData(Get.arguments),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      var data = snapshot.data!.data() as Map<String, dynamic>;
                      controller.nameC.text = data["name"];
                      controller.typeC.text = data["type"];
                      controller.balanceC.text = data["balance"];
                      return Padding(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          children: [
                            TextField(
                              controller: controller.nameC,
                              autocorrect: false,
                              textInputAction: TextInputAction.next,
                              decoration:
                                  InputDecoration(labelText: "Nama Tabungan"),
                            ),
                            TextField(
                              controller: controller.typeC,
                              autocorrect: false,
                              textInputAction: TextInputAction.next,
                              decoration:
                                  InputDecoration(labelText: "Type Tabungan"),
                            ),
                            TextField(
                              controller: controller.balanceC,
                              autocorrect: false,
                              textInputAction: TextInputAction.next,
                              decoration: InputDecoration(labelText: "Saldo"),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            ElevatedButton(
                                onPressed: () => controller.editEscrow(
                                      controller.nameC.text,
                                      controller.typeC.text,
                                      controller.balanceC.text,
                                      Get.arguments,
                                    ),
                                child: Text("Simpan"))
                          ],
                        ),
                      );
                    }
                    return Center(child: CircularProgressIndicator());
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
