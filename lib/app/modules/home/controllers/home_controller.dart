import 'package:get/get.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class HomeController extends GetxController {
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  Stream<QuerySnapshot<Object?>> streamDataEscrow() {
    CollectionReference escrows = firestore.collection("escrows");
    return escrows.snapshots();
  }

  void deleteEscrow(String docID) {
    DocumentReference docRef = firestore.collection("escrows").doc(docID);
    try {
      Get.defaultDialog(
          title: "Hapus Data",
          middleText: "Apakah data akan dihapus ?",
          onConfirm: () async {
            await docRef.delete();
            Get.back();
          },
          textConfirm: "Yes",
          textCancel: "No");
    } catch (e) {
      print(e);
      Get.defaultDialog(
        title: "Terjadi Kesalahan",
        middleText: "Tidak dapat menghapus data ini",
      );
    }
  }

  Stream<QuerySnapshot<Object?>> streamDataTransaction() {
    CollectionReference transactions = firestore.collection("transactions");
    return transactions.snapshots();
  }

  void deleteTransaction(String docID) {
    DocumentReference docRef = firestore.collection("transactions").doc(docID);
    try {
      Get.defaultDialog(
          title: "Hapus Data",
          middleText: "Apakah data akan dihapus ?",
          onConfirm: () async {
            await docRef.delete();
            Get.back();
          },
          textConfirm: "Yes",
          textCancel: "No");
    } catch (e) {
      print(e);
      Get.defaultDialog(
        title: "Terjadi Kesalahan",
        middleText: "Tidak dapat menghapus data ini",
      );
    }
  }
}
