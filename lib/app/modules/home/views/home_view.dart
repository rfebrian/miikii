import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:miikii/app/routes/app_pages.dart';
import '../../../controllers/auth_controller.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  final authC = Get.find<AuthController>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      child: Container(
        height: MediaQuery.of(context).size.height / 1,
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: IconButton(
                      alignment: Alignment.center,
                      onPressed: () {},
                      icon: Icon(Icons.person)),
                ),
                Container(
                  height: MediaQuery.of(context).size.height / 8,
                  width: MediaQuery.of(context).size.width / 3,
                  decoration: BoxDecoration(
                      color: Color(0xff65B73B),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(30),
                          bottomRight: Radius.circular(30))),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: IconButton(
                      alignment: Alignment.center,
                      onPressed: () => authC.logout(),
                      icon: Icon(Icons.logout)),
                )
              ],
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height / 50,
            ),
            Container(
                height: MediaQuery.of(context).size.height / 5.5,
                child: _Escrow()),
            Container(),
            Container(
              height: MediaQuery.of(context).size.height / 2,
              child: _TransactionData(),
            ),
          ],
        ),
      ),
    );
  }

  _Escrow() {
    return Scaffold(
      body: StreamBuilder<QuerySnapshot<Object?>>(
          stream: controller.streamDataEscrow(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.active) {
              var listAllDocs = snapshot.data!.docs;
              return ListView.builder(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: listAllDocs.length,
                  itemBuilder: (context, index) => Container(
                      width: MediaQuery.of(context).size.width / 1.3,
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: Color(0xffFF7438),
                          borderRadius: BorderRadius.all(Radius.circular(30))),
                      child: Stack(
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 15, left: 30),
                            child: Text(
                              "${(listAllDocs[index].data() as Map<String, dynamic>)["name"]}",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 25,
                                  fontWeight: FontWeight.w800),
                            ),
                          ),
                          Container(
                            alignment: Alignment.topRight,
                            padding: EdgeInsets.only(top: 22, right: 30),
                            child: Text(
                              "${(listAllDocs[index].data() as Map<String, dynamic>)["type"]}",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(left: 30, top: 20),
                            child: Text(
                              "Rp. ${(listAllDocs[index].data() as Map<String, dynamic>)["balance"]}",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ],
                      )));
            }
            return Center(child: CircularProgressIndicator());
          }),
    );
  }

  _TransactionData() {
    var streamBuilder = StreamBuilder<QuerySnapshot<Object?>>(
        stream: controller.streamDataTransaction(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.active) {
            var listAllDocs = snapshot.data!.docs;
            return ListView.builder(
                itemCount: listAllDocs.length,
                itemBuilder: (context, index) => ListTile(
                      onTap: () => Get.toNamed(Routes.EDIT_TRANSACTION,
                          arguments: listAllDocs[index].id),
                      title: Text(
                        "${(listAllDocs[index].data() as Map<String, dynamic>)["name"]}",
                      ),
                      subtitle: Text(
                        "${(listAllDocs[index].data() as Map<String, dynamic>)["type"]}",
                      ),
                      trailing: IconButton(
                          onPressed: () => controller
                              .deleteTransaction(listAllDocs[index].id),
                          icon: Icon(Icons.delete)),
                    ));
          }
          return Center(child: CircularProgressIndicator());
        });
    return Scaffold(
      body: streamBuilder,
    );
  }
}
