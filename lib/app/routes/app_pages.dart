import 'package:get/get.dart';
import 'package:miikii/app/modules/auth/login/bindings/login_binding.dart';
import 'package:miikii/app/modules/auth/login/views/login_view.dart';
import 'package:miikii/app/modules/auth/reset_password/bindings/reset_password_binding.dart';
import 'package:miikii/app/modules/auth/reset_password/views/reset_password_view.dart';
import 'package:miikii/app/modules/auth/signup/bindings/signup_binding.dart';
import 'package:miikii/app/modules/auth/signup/views/signup_view.dart';
import 'package:miikii/app/modules/dashboard/bindings/dashboard_binding.dart';
import 'package:miikii/app/modules/dashboard/views/dashboard_view.dart';
import 'package:miikii/app/modules/escrows/add_escrow/bindings/add_escrow_binding.dart';
import 'package:miikii/app/modules/escrows/add_escrow/views/add_escrow_view.dart';
import 'package:miikii/app/modules/escrows/edit_escrow/bindings/edit_escrow_binding.dart';
import 'package:miikii/app/modules/escrows/edit_escrow/views/edit_escrow_view.dart';
import 'package:miikii/app/modules/escrows/view_escrow/bindings/view_escrow_binding.dart';
import 'package:miikii/app/modules/escrows/view_escrow/views/view_escrow_view.dart';
import 'package:miikii/app/modules/transactions/add_transaction/bindings/add_transaction_binding.dart';
import 'package:miikii/app/modules/transactions/add_transaction/views/add_transaction_view.dart';
import 'package:miikii/app/modules/transactions/edit_transaction/bindings/edit_transaction_binding.dart';
import 'package:miikii/app/modules/transactions/edit_transaction/views/edit_transaction_view.dart';
import 'package:miikii/app/modules/transactions/view_transaction/bindings/view_transaction_binding.dart';
import 'package:miikii/app/modules/transactions/view_transaction/views/view_transaction_view.dart';
import 'package:miikii/app/modules/user/bindings/user_binding.dart';
import 'package:miikii/app/modules/user/views/user_view.dart';

import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.DASHBOARD;

  static final routes = [
    GetPage(
      name: _Paths.DASHBOARD,
      page: () => DashboardView(),
      binding: DashboardBinding(),
    ),
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.SIGNUP,
      page: () => SignupView(),
      binding: SignupBinding(),
    ),
    GetPage(
      name: _Paths.RESET_PASSWORD,
      page: () => ResetPasswordView(),
      binding: ResetPasswordBinding(),
    ),
    GetPage(
      name: _Paths.EDIT_TRANSACTION,
      page: () => EditTransactionView(),
      binding: EditTransactionBinding(),
    ),
    GetPage(
      name: _Paths.ADD_TRANSACTION,
      page: () => AddTransactionView(),
      binding: AddTransactionBinding(),
    ),
    GetPage(
      name: _Paths.VIEW_TRANSACTION,
      page: () => ViewTransactionView(),
      binding: ViewTransactionBinding(),
    ),
    GetPage(
      name: _Paths.USER,
      page: () => UserView(),
      binding: UserBinding(),
    ),
    GetPage(
      name: _Paths.ADD_ESCROW,
      page: () => AddEscrowView(),
      binding: AddEscrowBinding(),
    ),
    GetPage(
      name: _Paths.EDIT_ESCROW,
      page: () => EditEscrowView(),
      binding: EditEscrowBinding(),
    ),
    GetPage(
      name: _Paths.VIEW_ESCROW,
      page: () => ViewEscrowView(),
      binding: ViewEscrowBinding(),
    )
  ];
}
