part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const DASHBOARD = _Paths.DASHBOARD;
  static const HOME = _Paths.HOME;
  static const LOGIN = _Paths.LOGIN;
  static const SIGNUP = _Paths.SIGNUP;
  static const RESET_PASSWORD = _Paths.RESET_PASSWORD;
  static const ADD_ESCROW = _Paths.ADD_ESCROW;
  static const EDIT_ESCROW = _Paths.EDIT_ESCROW;
  static const VIEW_ESCROW = _Paths.VIEW_ESCROW;
  static const EDIT_TRANSACTION = _Paths.EDIT_TRANSACTION;
  static const ADD_TRANSACTION = _Paths.ADD_TRANSACTION;
  static const VIEW_TRANSACTION = _Paths.VIEW_TRANSACTION;
  static const USER = _Paths.USER;
}

abstract class _Paths {
  _Paths._();
  static const DASHBOARD = '/dashboard';
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const SIGNUP = '/signup';
  static const RESET_PASSWORD = '/reset-password';
  static const ADD_ESCROW = '/add-escrow';
  static const EDIT_ESCROW = '/edit-escrow';
  static const VIEW_ESCROW = '/view-escrow';
  static const EDIT_TRANSACTION = '/edit-transaction';
  static const ADD_TRANSACTION = '/add-transaction';
  static const VIEW_TRANSACTION = '/view-transaction';
  static const USER = '/user';
}
