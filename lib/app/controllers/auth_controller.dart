import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:miikii/app/routes/app_pages.dart';

class AuthController extends GetxController {
  FirebaseAuth auth = FirebaseAuth.instance;

  Stream<User?> get streamAuthStatus => auth.authStateChanges();

  void login(String email, String password) async {
    try {
      UserCredential myUser = await auth.signInWithEmailAndPassword(
          email: email, password: password);
      if (myUser.user!.emailVerified) {
        Get.offAllNamed(Routes.DASHBOARD);
      } else {
        Get.defaultDialog(
            title: "Verification Email",
            middleText: "Anda harus melakukan verifikasi terlebih dahulu",
            onConfirm: () async {
              await myUser.user!.sendEmailVerification();
              Get.back();
            },
            textConfirm: "Kirim Ulang",
            textCancel: "Kembali");
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        Get.defaultDialog(
            title: "Terjadi Kesalahan",
            middleText: "User & Email tidak terdaftar pada aplikasi",
            onConfirm: () {
              Get.back();
            },
            textConfirm: "Ok");
      } else if (e.code == 'wrong-password') {
        Get.defaultDialog(
            title: "Terjadi Kesalahan",
            middleText: "Username / Password Salah",
            onConfirm: () {
              Get.back();
            },
            textConfirm: "Ok");
      }
    } catch (e) {
      Get.defaultDialog(
        title: "Terjadi Keselahan",
        middleText: "Tidak Dapat Login Menggunakan Akun Ini",
      );
    }
  }

  void logout() async {
    await FirebaseAuth.instance.signOut();
    Get.offAllNamed(Routes.LOGIN);
  }

  void signup(String email, String password) async {
    try {
      UserCredential myUser = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      await myUser.user!.sendEmailVerification();
      Get.defaultDialog(
        title: "Verification Email",
        middleText: "Cek Email Anda $email.",
        onConfirm: () {
          Get.back();
          Get.back();
        },
        textConfirm: "Ya, Saya akan Cek Email",
      );
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        Get.defaultDialog(
          title: "Terjadi Keselahan",
          middleText: "Password terlalu lemah",
        );
      } else if (e.code == 'email-already-in-use') {
        Get.defaultDialog(
          title: "Terjadi Keselahan",
          middleText: "Email Sudah Terdaftar",
        );
      }
    } catch (e) {
      Get.defaultDialog(
        title: "Terjadi Keselahan",
        middleText: "Tidak Dapat Mendaftarkan akun ini",
      );
    }
  }

  void resetPassword(String email) async {
    if (email != "" && GetUtils.isEmail(email)) {
      try {
        auth.sendPasswordResetEmail(email: email);
        Get.defaultDialog(
            title: "Berhasil",
            middleText: "Link Reset Password Sudah di kirim ke $email.",
            onConfirm: () {
              Get.back();
              Get.back();
            },
            textConfirm: "Ok");
      } catch (e) {
        Get.defaultDialog(
          title: "Terjadi Kesalahan",
          middleText: "Tidak dapat mengirimkan reset password",
        );
      }
    } else {
      Get.defaultDialog(
        title: "Terjadi Kesalahan",
        middleText: "Email tidak valid.",
      );
    }
  }
}
