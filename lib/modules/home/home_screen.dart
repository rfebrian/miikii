import 'package:flutter/material.dart';
import 'package:miikii/modules/home/tabs/buy_tab.dart';
import 'package:miikii/modules/home/tabs/main_tab.dart';
import 'package:miikii/modules/home/tabs/sell_tab.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _HomeScreenState extends State<HomeScreen> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    MainTab(),
    SellTab(),
    BuyTab(),
    Text(
      'Index 2: School',
      style: optionStyle,
    ),
    Text(
      'Index 2: School',
      style: optionStyle,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      extendBody: true,
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: bottomNavigationBar,
    );
  }

  Widget get bottomNavigationBar {
    return ClipRRect(
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(40),
        topRight: Radius.circular(40),
      ),
      child: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              color: Color(0xFF73d1d3),
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.store_mall_directory_rounded,
              color: Color(0xFF73d1d3),
            ),
            label: 'Sell',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.local_grocery_store_rounded,
                color: Color(0xFF73d1d3)),
            label: 'Buy',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.local_offer_rounded, color: Color(0xFF73d1d3)),
            label: 'Book',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings, color: Color(0xFF73d1d3)),
            label: 'Setting',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: const Color(0xFF73d1d3),
        onTap: _onItemTapped,
      ),
    );
  }
}
