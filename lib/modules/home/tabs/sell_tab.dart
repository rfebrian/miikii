import 'package:flutter/material.dart';

class SellTab extends StatefulWidget {
  const SellTab({Key? key}) : super(key: key);

  @override
  _SellTabState createState() => _SellTabState();
}

class _SellTabState extends State<SellTab> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _searchBox(),
        SizedBox(
          height: 30,
        ),
        _categoryItem(),
        SizedBox(
          height: 30,
        ),
        _itemList(),
      ],
    );
  }
}

Widget _searchBox() {
  return Container(
    margin: EdgeInsets.only(top: 70),
    width: 320,
    height: 50,
    decoration: BoxDecoration(
        color: Colors.grey.shade200.withOpacity(0.2),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15), bottomRight: Radius.circular(15))),
    child: TextField(
      decoration: InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.all(20),
          hintText: 'Search',
          suffixIcon: Icon(
            Icons.search,
            color: Colors.black,
          )),
    ),
  );
}

Widget _categoryItem() {
  return Container(
    width: 320,
    height: 22,
    child: ListView(
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        Container(
          width: 83,
          height: 22,
          alignment: Alignment.center,
          child: Text(
            "T-Shirt",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          width: 83,
          height: 22,
          alignment: Alignment.center,
          child: Text(
            "Shirt",
            style: TextStyle(fontSize: 20, color: Colors.grey.shade300),
          ),
        ),
        Container(
          width: 83,
          height: 22,
          alignment: Alignment.center,
          child: Text(
            "Sweater",
            style: TextStyle(fontSize: 20, color: Colors.grey.shade300),
          ),
        ),
        Container(
          width: 83,
          height: 22,
          alignment: Alignment.center,
          child: Text(
            "Dress",
            style: TextStyle(fontSize: 20, color: Colors.grey.shade300),
          ),
        ),
        Container(
          width: 83,
          height: 22,
          alignment: Alignment.center,
          child: Text(
            "Pants",
            style: TextStyle(fontSize: 20, color: Colors.grey.shade300),
          ),
        ),
      ],
    ),
  );
}

Widget _itemList() {
  return Container(
    color: Colors.grey.shade200.withOpacity(0.3),
    child: SafeArea(
        child: SingleChildScrollView(
      child: Column(
        children: [
          Container(
            width: 350,
            child: Row(
              children: [
                Expanded(
                    child: Container(
                  padding: EdgeInsets.only(left: 10),
                  child: Text(
                    "10 Product",
                    style: TextStyle(),
                  ),
                )),
                Expanded(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      "10 Product",
                      style: TextStyle(),
                    ),
                    IconButton(
                        onPressed: () {},
                        icon: Icon(Icons.arrow_drop_down_rounded)),
                  ],
                ))
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 400,
            width: 310,
            child: GridView.count(
              crossAxisCount: 2,
              scrollDirection: Axis.vertical,
              children: <Widget>[
                Container(
                  child: Card(
                    color: Colors.grey.shade200.withOpacity(0.2),
                  ),
                ),
                Container(
                  child: Card(
                    color: Colors.grey.shade200.withOpacity(0.2),
                  ),
                ),
                Container(
                  child: Card(
                    color: Colors.grey.shade200.withOpacity(0.2),
                  ),
                ),
                Container(
                  child: Card(
                    color: Colors.grey.shade200.withOpacity(0.2),
                  ),
                ),
                Container(
                  child: Card(
                    color: Colors.grey.shade200.withOpacity(0.2),
                  ),
                ),
                Container(
                  child: Card(
                    color: Colors.grey.shade200.withOpacity(0.2),
                  ),
                ),
                Container(
                  child: Card(
                    color: Colors.grey.shade200.withOpacity(0.2),
                  ),
                ),
                Container(
                  child: Card(
                    color: Colors.grey.shade200.withOpacity(0.2),
                  ),
                ),
                Container(
                  child: Card(
                    color: Colors.grey.shade200.withOpacity(0.2),
                  ),
                ),
                Container(
                  child: Card(
                    color: Colors.grey.shade200.withOpacity(0.2),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    )),
  );
}
