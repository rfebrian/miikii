import 'package:flutter/material.dart';
import 'package:miikii/widgets/chart.dart';

class MainTab extends StatelessWidget {
  const MainTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Column(
        children: [
          Container(
              width: MediaQuery.of(context).size.width / 1,
              child: _greatings()),
          SizedBox(height: MediaQuery.of(context).size.height / 30),
          Container(
              width: MediaQuery.of(context).size.width / 1,
              child: _listBalance()),
          SizedBox(height: MediaQuery.of(context).size.width / 30),
          Chart()
        ],
      ),
    );
  }
}

Widget _greatings() {
  return Container(
    alignment: Alignment.topLeft,
    padding: EdgeInsets.only(left: 40, top: 57),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Good Night",
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 9,
        ),
        Text("Rizki Febriansyah !",
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold))
      ],
    ),
  );
}

Widget _listBalance() {
  return Container(
    padding: EdgeInsets.only(left: 41),
    color: Colors.transparent,
    height: 90.0,
    child: ListView(
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.grey.shade100.withOpacity(0.2)),
          width: 284.0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  alignment: Alignment.topRight,
                  padding: EdgeInsets.only(top: 5, right: 30),
                  child: Text("Available",
                      style: TextStyle(
                          fontSize: 12, fontWeight: FontWeight.w600))),
              Container(
                  padding: EdgeInsets.only(left: 20),
                  child: Text("Balance",
                      style: TextStyle(
                          fontSize: 20, fontWeight: FontWeight.w300))),
              Container(
                  padding: EdgeInsets.only(left: 20, top: 3),
                  child: Text("Rp. 1,500,000",
                      style:
                          TextStyle(fontSize: 28, fontWeight: FontWeight.w800)))
            ],
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.grey.shade100.withOpacity(0.2)),
          width: 284.0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  alignment: Alignment.topRight,
                  padding: EdgeInsets.only(top: 5, right: 30),
                  child: Text("Debit",
                      style: TextStyle(
                          fontSize: 12, fontWeight: FontWeight.w600))),
              Container(
                  padding: EdgeInsets.only(left: 20),
                  child: Text("Balance",
                      style: TextStyle(
                          fontSize: 20, fontWeight: FontWeight.w300))),
              Container(
                  padding: EdgeInsets.only(left: 20, top: 3),
                  child: Text("Rp. 1,471,000",
                      style:
                          TextStyle(fontSize: 28, fontWeight: FontWeight.w800)))
            ],
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.grey.shade100.withOpacity(0.2)),
          width: 284.0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  alignment: Alignment.topRight,
                  padding: EdgeInsets.only(top: 5, right: 30),
                  child: Text("Credit",
                      style: TextStyle(
                          fontSize: 12, fontWeight: FontWeight.w600))),
              Container(
                  padding: EdgeInsets.only(left: 20),
                  child: Text("Balance",
                      style: TextStyle(
                          fontSize: 20, fontWeight: FontWeight.w300))),
              Container(
                  padding: EdgeInsets.only(left: 20, top: 3),
                  child: Text("Rp. 578,000",
                      style:
                          TextStyle(fontSize: 28, fontWeight: FontWeight.w800)))
            ],
          ),
        ),
      ],
    ),
  );
}
